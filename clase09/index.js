const mascotas = ['Perros', 'Gatos', 'Aves', 'Conejos', 'Peces'];

mascotas.push('Ratones');

console.log(mascotas);

mascotas.forEach((mascota, indice) => {
    console.log(`Indice: ${indice} valor: ${mascota}`);
});
