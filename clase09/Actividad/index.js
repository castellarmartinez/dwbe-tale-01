const calculadora = require('./calculadora')
const input = require('readline-sync')

let operacion;

do
{
    operacion = input.question('\nOperacion a realizar: \n1. Suma\n2. resta\n3. Multiplicacion\n4. Division\n0. Salir\nOpcion: ')

    switch(operacion)
    {
        case '1':
            const sumando1 = parseInt(input.question('Ingrese el primer sumando: '))
            const sumando2 = parseInt(input.question('Ingrese el sedundo sumando: '))
            const suma = calculadora.suma(sumando1, sumando2)
            console.log(`La suma de ${sumando1} y ${ sumando2} es: ${suma}`)
        break

        case '2':
            const minuendo = parseInt(input.question('Ingrese el minuendo: '))
            const sustraendo = parseInt(input.question('Ingrese el sustraendo: '))
            const resta = calculadora.resta(minuendo, sustraendo)
            console.log(`La resta de ${minuendo} y ${ sustraendo} es: ${resta}`)
        break

        case '3':
            const multiplicando = parseInt(input.question('Ingrese el multiplicando: '))
            const multiplicador = parseInt(input.question('Ingrese el multiplicador: '))
            const multiplicacion = calculadora.multiplicacion(multiplicando, multiplicador)
            console.log(`La multiplicación de ${multiplicando} y ${ multiplicador} es: ${multiplicacion}`)
        break

        case '4':
            const dividendo = parseInt(input.question('Ingrese el dividendo: '))
            const divisor = parseInt(input.question('Ingrese el divisor: '))
            const diviision = calculadora.division(dividendo, divisor)
            console.log(`La división de ${dividendo} y ${divisor} es: ${diviision}`)
        break

        case '0':
            console.log('Gracias por usar la calculadora.\n')
        break

        default:
            console.log('La opción ingresada no es válida.')
        break
    }
}while('0' != operacion)
                    
