document.getElementById("titulo").innerHTML = "Titulo por JS"

class Perro
{
    constructor(nombre, raza, color, altura, edad, estadoAdopcion)
    {
        this.nombre = nombre;
        this.raza = raza;
        this.color = color;
        this.altura = altura;
        this.edad = edad;
        this.estadoAdopcion = estadoAdopcion;
    }

    modificarEstadoDeAdopcion(estadoAdopcion)
    {
        this.estadoAdopcion = estadoAdopcion;
    }

    informarEstadoDeAdopcion()
    {
        return this.estadoAdopcion;
    }
}

let perros = [];

// perros.push(new Perro("Firulais", "Criollo", "Amarillo", "42cm",
//                     "6 años", "En adopción"));
// perros.push(new Perro("Pluto", "Pitbull", "Negro", "33cm",
//                     "2 años", "En adopción"));
// perros.push(new Perro("Tarzán", "Dálmata", "Blanco con negro", 
//                     "57cm", "1 años", "No adoptado"));

do
{
    let perro = new Perro(prompt("Nombre del perro"), 
                        prompt("Raza del perro"), 
                        prompt("Color del perro"),
                        prompt("Altura del perro"),
                        prompt("Edad del perro"),
                        prompt("Estado de adopción"));
    
    perros.push(perro);
}while(window.confirm("¿Desea agregar otro perro?"));

console.log("Todos los perros: ");

perros.forEach((e) => {
    console.log(e);
});

console.log("\n\nPerros en adopción: ");
const enAdopcion = perros.filter(perro => perro.informarEstadoDeAdopcion() === "En adopción")

enAdopcion.forEach((e) => {
    console.log(e);
});

console.log("\n\nPerros en proceso de adopción: ");
const enProcesoAdopcion = perros.filter(perro => perro.informarEstadoDeAdopcion() === "En proceso de adopción")

enProcesoAdopcion.forEach((e) => {
    console.log(e);
});

console.log("\n\nPerros no adoptados: ");
const noAdoptado = perros.filter(perro => perro.informarEstadoDeAdopcion() === "No adoptado")

noAdoptado.forEach((e) => {
    console.log(e);
});


