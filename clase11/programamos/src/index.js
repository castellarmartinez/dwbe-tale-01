const express = require('express');
const app = express();
const { obtenerTelefonos, obtenerMitadTelefonos, obtenerMayorPrecio, obtenerMenorPrecio, obtenerGama } = require('./models/Telefonos');

app.get('/telefonos', (req, res) => {
    res.json(obtenerTelefonos());
});

app.get('/mitadtelefonos', (req, res) => {
    res.json(obtenerMitadTelefonos());
});

app.get('/precioMayor', (req, res) => {
    res.json(obtenerMayorPrecio());
});

app.get('/precioMenor', (req, res) => {
    res.json(obtenerMenorPrecio());
});

app.get('/gama', (req, res) => {
    res.json(obtenerGama());
});

app.listen(3000, () => {
    console.log('Escuchando en el puerto 3000');
});