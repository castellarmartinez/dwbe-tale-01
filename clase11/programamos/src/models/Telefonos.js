const telefonos = [
    {
        marca: "Samsung",
        gama: "Alta",
        modelo: "S11",
        pantalla: "19:9",
        sistema_operativo: "Android",
        precio: 1200
    },
    {
        marca: "Iphone",
        modelo: "12 pro",
        gama: "Alta",
        pantalla: "OLED",
        sistema_operativo: "iOs",
        precio: 1500
    },
    {
        marca: "Xiaomi",
        modelo: "Note 10s",
        gama: "Baja",
        pantalla: "OLED",
        sistema_operativo: "Android",
        precio: 300
    },
    {
        marca: "LG",
        modelo: "LG el que sea",
        gama: "Media",
        pantalla: "OLED",
        sistema_operativo: "Android",
        precio: 800
    },
    {
        marca: "Realme",
        modelo: "7",
        gama: "Media",
        pantalla: "OLED",
        sistema_operativo: "Android",
        precio: 800
    }
];

const obtenerTelefonos = () => {
    return telefonos;
}

const obtenerMitadTelefonos = () => {
    const mitad = Math.round(telefonos.length / 2);
    const mitadTelefonos = [];

    for (let index = 0; index < mitad; index++) {
        const telefono = telefonos[index];
        mitadTelefonos.push(telefono);
    }
    return mitadTelefonos;
}

const obtenerPrecios = () => {
    let precios = [];

    telefonos.forEach(celulares => {
        precios.push(parseInt(celulares.precio))
    })

    precios = precios.sort((a, b) => a - b);
    return precios;
}

const obtenerMenorPrecio = () => {
    const precios = obtenerPrecios();
    return precios[0];
}

const obtenerMayorPrecio = () => {
    const precios = obtenerPrecios();
    return precios[precios.length - 1];
}

const obtenerGama = () => {
    const gamas = [[], [], []];

    telefonos.forEach(celulares => {
        if(celulares.gama === 'Baja')
        {
            gamas[0].push(celulares);
        }
        else if(celulares.gama === 'Media')
        {
            gamas[1].push(celulares);
        }
        else if(celulares.gama === 'Alta')
        {
            gamas[2].push(celulares);
        }
    });

    const gamasAgrupadas = gamas[0].concat(gamas[1], gamas[2]);
    return gamasAgrupadas;
}

module.exports = { obtenerTelefonos, obtenerMitadTelefonos, obtenerMayorPrecio, obtenerMenorPrecio, obtenerGama };