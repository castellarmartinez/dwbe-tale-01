
const alumnos = 
[
    {
        nombre: 'David', 
        edad: 26, 
        carrera: 'Backend'
    },

    {
        nombre: 'Emmanuel', 
        edad: 23, 
        carrera: 'Backend'
    },

    {
        nombre: 'Dulcinea', 
        edad: 21, 
        carrera: 'Frontend'
    },

    {
        nombre: 'Lucrecia', 
        edad: 24, 
        carrera: 'Backend'
    },

    {
        nombre: 'Pedro', 
        edad: 23, 
        carrera: 'Datascience'
    },
    
    {
        nombre: 'Arnedes', 
        edad: 25, 
        carrera: 'Frontend'
    }
];

const obtenerEstudiantes = () => {
    return alumnos;
}

const agregarEstudiantes = (estudiante, posicion) => {
    alumnos.splice(posicion - 1, 0, estudiante)
}

const modificarEstudiantes = (nuevoEstudiante, posicion) => {
    alumnos.splice(posicion - 1, 1, nuevoEstudiante)
}

const eliminarEstudiantes = (indice) => {
    alumnos.splice(indice - 1, 1);
}

module.exports = {obtenerEstudiantes, agregarEstudiantes, 
    modificarEstudiantes, eliminarEstudiantes};