const express = require('express');
//require('dotenv').config();
const app = express();
const {obtenerEstudiantes, agregarEstudiantes, 
    modificarEstudiantes, eliminarEstudiantes} = require('./estudiantes.js');

const PORT = process.env.PORT || 3000;

app.use(express.json());

app.get('/', (req, res) =>
{
    res.send('Hello world!')
})

app.get('/estudiantes', function (req, res) {
    let lista = '';

    obtenerEstudiantes().forEach((miembro, i) =>
    {
        lista += `Estudiante ${i + 1}: ${miembro.nombre}, ${miembro.edad} años, carrera: ${miembro.carrera}\n`;
    })

    res.send(lista);
});

app.post('/agregar', (req, res) => {
    const estudiante = req.body;
    const {indice} = req.query;

    agregarEstudiantes(estudiante, indice)
    res.send('Se agrega un estudiante')
});

app.put('/modificar', (req, res) => {
    const estudiante = req.body;
    const {indice} = req.query;

    modificarEstudiantes(estudiante, indice)
    res.send('Se modifica un estudiante')
});

app.delete('/eliminar', (req, res) => {
    const {indice} = req.query;
    
    eliminarEstudiantes(indice)
    res.send('Se agrega un estudiante')
});

app.listen(PORT, function () {
  console.log(`Escuchando el puerto ${PORT}!`);
});
