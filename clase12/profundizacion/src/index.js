const express = require('express');
const app = express();

app.use((req, res, next) => {
  console.log('Time:', Date.now().toLocaleString());
  next();
});

app.get('/', (req, res) => {
  res.send('Hola mundo')
  console.log('Pasamos el middleware')
});

app.listen(3000, () => {
  console.log('Escuchando desde el puerto 3000')
});

app.use('/user/:id', (req, res, next) => {
  console.log('Request Type:', req.method);
  next();
});

// app.get('/user/:id', function (req, res, next) {
//   res.send('USER');
// });

app.get('/user/:id', (req, res, next) => {
  console.log('ID:', req.params.id);
  next();
  }, (req, res, next) => {
  res.send('User Info');
});

// handler for the /user/:id path, which prints the user ID
app.get('/user/:id', function (req, res, next) {
  res.end(req.params.id);
});