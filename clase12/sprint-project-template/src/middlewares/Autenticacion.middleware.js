const { obtenerUsuarios } = require('../models/usuario.model');

const autenticacion = (usuario, contrasena) => {
    const usuarios = obtenerUsuarios().some(u => u.username === usuario && u.password === contrasena);
    if (usuarios) return true;
    else return false;
}

module.exports = autenticacion;