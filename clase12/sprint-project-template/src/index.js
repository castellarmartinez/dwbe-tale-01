const express = require('express');
const app = express();
const basicAuth = require('express-basic-auth');
const autenticacion = require('./middlewares/Autenticacion.middleware');

app.use(express.json());

a = {
    colombia: 'america', 
    argentina: 'river', 
    mexico: 'tigres'
};

app.get('/login', (req, res) => {
    res.write('Hola mundo\n');
    res.write(JSON.stringify(a))
    res.end()
});

app.post('/registrarUsuario', (req, res) => {
    res.json('no autenticacion');
});

app.use(basicAuth({ authorizer: autenticacion}));

const usuarioRoutes = require('./routes/usuario.route');

app.use('/usuarios', usuarioRoutes);

app.listen(3000, () => { console.log('Escuchando en el puerto 3000') });