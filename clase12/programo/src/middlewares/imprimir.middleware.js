function imprimirRequest(req, res, next) {
    console.log("Hora desde middleware:", Date.now());
    next();
}

function imprimirRequest2(req, res, next) {
    console.log("Path desde middleware2:", req.path);
    if (req.path === '/ejemplo2') {
        next();
    } else {
        res.status(401).json('No autorizado');
    }
}

function imprimirRequest3(req, res, next) {
    console.log("Path desde middleware3:", req.path);
    next();
}

module.exports = { imprimirRequest, imprimirRequest2, imprimirRequest3 }