const express = require('express');
const app = express();
const {isAdmin} = require('./middlewares/middlewares')
const port = 3000;

// Permite recibir parámetros en formato JSON.
app.use(express.json());

// Ruta a la cual solo deben ingresar usuarios administradores.
app.get('/dashboard', isAdmin, (req, res) => {
    res.send('You are an admin');
});

app.listen(port, () => {
    console.log(`Server listeting on port ${port}`)
});