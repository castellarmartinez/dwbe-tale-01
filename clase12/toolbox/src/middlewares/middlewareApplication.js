const logger = (req, res, next) => {
    console.log(`request HTTP method: ${req.method}`);   
    next(); 
}

module.exports = {logger}