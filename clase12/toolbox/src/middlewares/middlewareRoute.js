const esAdministrador = (req, res, next) => {  
    const usuarioAdministrador = false;   
    if(usuarioAdministrador ) 
    {
        console.log('El usuario está correctamente logueado.');
        next();
    } 
    else 
    {  
        res.send('No está logueado');   
    }
};

module.exports = {esAdministrador};