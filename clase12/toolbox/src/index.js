const express = require('express');
const app = express();
const {logger} = require('./middlewares/middlewareApplication')
const {esAdministrador} = require('./middlewares/middlewareRoute')

app.listen(5000, () => console.log("listening on 5000"));

app.use(logger);

app.get('/estudiantes', (req, res) => {
    res.send([
        { id: 1, nombre: "Lucas", edad: 35 }
    ])
}); 
  
app.post('/estudiantes', (req, res) => {
    res.status(201).send();
});

app.get('/alumnos', esAdministrador, (req, res) => {
    res.send([
        { id: 1, nombre: "Lucas", edad: 35 }
    ])
}); 
  
app.post('/alumnos', esAdministrador, (req, res) => {
    res.status(201).send();
});