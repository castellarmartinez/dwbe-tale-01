const coolImages = require("cool-images");
const fs = require('fs');

let url = coolImages.many(600, 800, 25);

url.forEach((image, index) =>
{
    const mensaje = `Imagen ${index + 1}: ${image}\n`;
    fs.appendFileSync('listaImages.txt', mensaje);
})
