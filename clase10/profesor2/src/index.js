let moment = require('moment')
console.log('Horario local: ' + moment().format)
console.log('Horario UTC: ' + moment.utc().format)
const diferencia = moment().hours - moment.utc().hour;
console.log('UTC offset: ', + (moment().utcOffset() / 60))
const fechaIncial = moment('2060-01-01')

if(fechaIncial.isBefore('2050-01-01'))
{
    console.log('La fecha inicial es anterior al año 2050')
}
else
{
    console.log('La fecha inicial es posterior al año 2050')
}